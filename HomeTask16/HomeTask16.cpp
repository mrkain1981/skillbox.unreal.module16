﻿// HomeTask16.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <ctime>

int main()
{
    const int N = 11;
    std::cout << "Hello World!\n";

    int ArrInt[N];
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            ArrInt[i, j] = i + j;
            std::cout << ' ' << ArrInt[i, j];
        }
        std::cout << '\n';
    }

    time_t now = time(0);

    struct tm timeinfo;
    localtime_s(&timeinfo, &now);

    int iday = timeinfo.tm_yday;
    

    int iSum = 0;
    int iDev = iday % N;
    for (int j = 0; j < N; j++) 
    {
        // В целом нужно, конечно, ставить проверку, что остаток от деления текущего дня календаря на N не будет выходить за индекс массива, но для ДЗ поленился (но осознаю опасность :) )
        iSum += ArrInt[iDev, j];
    }
    std::cout << iDev << "   SUM = " << iSum;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
